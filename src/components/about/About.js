import React, { useState, useEffect } from "react";
import axios from "axios";

export default function About() {
  const [flagName, setFlagName] = useState()
  const [flagStatus, setFlagStatus] = useState()

  useEffect(() => {
    fetchData()
  }, []);

  const fetchData = async () => {
    const url ='https://gitlab.com/api/v4/projects/45045755/feature_flags'
    const res = await axios.get(url, {
      headers: {
        'PRIVATE-TOKEN': `glpat-2zFyc1ARkMSyUoCuRhzf`
      }
  });
    setFlagName(res.data[0].name)
    setFlagStatus(res.data[0].active?'true':'false')

  }
  return (
    <div>
      {flagStatus? <h1>Welcome to My Cool App {flagName}- {flagStatus}</h1>:<h1>Welcome to My Cool App {flagName}- {flagStatus}</h1>}
    </div>
  );
}